# blockchain_btc

#### 介绍
PHP实现的BTC比特币充值，区块高度同步

#### 安装教程

1.  使用tp5.0.24框架，详情见 http://www.thinkphp.cn/

#### 使用说明

1.  http://****网址/index.php/index/btc/height  获取区块高度
2.  http://****网址/index.php/index/btc/sure    充值确认

#### 打赏地址

1.  以太地址：0xa9937d80d5d92861176085fedd6179F8B57C4E48
2.   BTC地址：1EDx8qVCYTpeEroip8nQvqMuHk8rG3cYem


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
